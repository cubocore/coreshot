/*
	*
	* This file is a part of CoreShot.
	* A screen capture utility for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#include <QApplication>
#include <QCommandLineParser>

#include "coreshotdialog.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName("CoreShot");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CoreShot");
    app.setQuitOnLastWindowClosed(true);

	QCommandLineParser cliparser;
	cliparser.addHelpOption();         // Help
	cliparser.addVersionOption();      // Version

	/* Description */
    cliparser.setApplicationDescription("A simple screenshot application written in Qt6 for X11");
	cliparser.addOptions( {
		{ { "fullscreen",    "f" }, "Capture the screenshot of the full screen" },
		{ { "active-window", "w" }, "Capture the screenshot of the active window" },
		{ { "selection",     "s" }, "Capture a part of the screen selected by the user" },
		{ { "delay",         "d" }, "Set a delay(sec) on when the capture takes place", "delay" },
	}
						);
	cliparser.addPositionalArgument("file", "The file to which the screenshot is written.");

	cliparser.process(app);

	QString file = cliparser.positionalArguments().value(0);
	int delay = 0;

	if (cliparser.isSet("delay")) {
		delay = cliparser.value("delay").toInt();
	}

	coreshotdialog gui(file, delay);

	if (cliparser.isSet("fullscreen")) {
		gui.capture(coreshotdialog::FullScreen);
	} else if (cliparser.isSet("active-window")) {
		gui.capture(coreshotdialog::ActiveWindow);
	} else if (cliparser.isSet("selection")) {
		gui.capture(coreshotdialog::SelectArea);
	} else {
		gui.show();
	}

	return app.exec();
}
